#include <iostream>
#include <math.h>

using namespace std;

int main (){

    //Definicion de variables
    int N,resultado,number;
    resultado = 0;

    //Tamaño del vector
    cout << "Ingrese la cantidad de numeros a ingresar" << endl;
    cin >> N;
    int suma[N];

    //Se agregan los numeros ingresados al vector 
    for (int i=0;i<N;i++){
        cout << "Ingrese un numero" << endl;
        cin >> number;
        suma[i] = number;
    }

    //Suma de los numeros ingresados en el arreglo
    for(int i=0;i<N;i++){
        resultado += pow(suma[i], 2);
    }

    //Se muestran los resultados
    cout << "La suma de los numeros ingresados al cuadrado es: " << resultado << endl << endl;

    return 0;
}
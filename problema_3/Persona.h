#ifndef PERSONA_H
#define PERSONA_H

#include <iostream>

using namespace std;

class Persona{
    
    private:
        string nombre, telefono;
		int saldo = 0;
        bool moroso = false;
    
    public:

        //CONSTRUCTOR 
        Persona();
        Persona(string nombre, string telefono, int saldo, bool m);
        void datos();

        //Seters
        void set_nombre(string nombre);
        void set_telefono(string telefono);
        void set_saldo(int saldo);
        void set_moroso(bool m);
    
        //Geters
        string get_nombre();
        string get_telefono();
        int get_saldo();
        bool get_moroso();       
};
#endif
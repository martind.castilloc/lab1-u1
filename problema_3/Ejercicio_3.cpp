#include <iostream>

#include "Persona.h"

using namespace std;

void menu(){
	
    Persona info = Persona();
    int opc;
	int i = 0;
	
	// Función impresión de menú inicial del programa
	do{
		cout << "\t     ---------------------------- " << endl;
		cout << "\t     ***** MENÚ DE OPCIONES ***** " << endl;
		cout << "\t     ---------------------------- " << endl;
		cout << "\n  [1]  > Ingreso de datos." << endl;
		cout << "  [2]  > Salir del programa." << endl;
		cout << "\n - elija una opción: ";

		cin >> opc;
		cin.ignore();

		if (opc == 1){

            info.datos();           
            i++;

		} else if (opc == 2){
			
			cout << "\n\t¡Se ha cerrado el programa correctamente!\n" << endl;
			break;

		} else{
			
			cout << "\t--------------------------------------" << endl;
			cout << "\tValor Invalido, ingrese una opcion valida" << endl;
			cout << "\t--------------------------------------\n" << endl;
		}
	} while(i<1);
}

int main(){
    
    menu();

    return 0;
}

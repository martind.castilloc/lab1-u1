#include <iostream>

#include "Persona.h"

using namespace std;

Persona::Persona(){
	this->nombre = "S/N";
	this->telefono = "S/N";
	this->saldo = 0;
	this->moroso = false;
	}

Persona::Persona(string nombre, string telefono, int saldo, bool mor){
	this->nombre = nombre;
	this->telefono = telefono;
	this->saldo = saldo;
	this->moroso = mor;
}

//SETERS
void Persona::set_nombre(string nombre){
	this->nombre = nombre;
}

void Persona::set_telefono(string telefono){
	this->telefono = telefono;
}

void Persona::set_saldo(int saldo){
	this->saldo = saldo;
}

void Persona::set_moroso(bool mor){
	this->moroso = mor;
}

//GETERS
string Persona::get_nombre(){
	return this->nombre;
}

string Persona::get_telefono(){
	return this->telefono;
}

int Persona::get_saldo(){
	return this->saldo;
}

bool Persona::get_moroso(){
	return this->moroso;
}


void Persona::datos(){

    string nombre, telefono;
    char mo;
	int resp, opc, saldo, c_personas, cont;
	bool moroso;
    cont = 0;

    cout << "Ingrese la cantidad de personas" << endl;
	cin >> c_personas;
	
	//Se define el tamaño del vector con la cantidad de personas ingresadas.
	Persona datos[c_personas];

    while (cont<c_personas){

		//Se piden los datos de la persona.        
		cout << "Ingrese el nombre de la persona..." << endl;
		cin >> nombre;
		cout << "Ingrese el telefono de la persona..." << endl;
		cin >> telefono;
		cout << "Ingrese el saldo de la persona" << endl;
		cin >> saldo;
		cout << "¿La persona tiene deudas pendientes?" << endl
			<< "Ingrese m si la persona tiene deudas" << endl
			<< "Ingrese n si no debe deudas" << endl;
        cin >> mo;
			
		if (mo == 'm'){
			moroso = true;
		}
			
		else if(mo == 'n'){
			moroso = false;
		}

        //Se instancia de objeto de tipo Persona.
		Persona per = Persona(nombre, telefono, saldo, moroso);
			
		//Se añade a la lista los datos de la persona.
		datos[cont] = per;
			
		cont++;
			
    }

    //Se imprimen los datos ingresados de cada persona.
    for(int i=0;i<c_personas;i++) {
        cout << "\n  >  Estado financiero del cliente(a): " << datos[i].get_nombre() << endl;
        cout << "\n  >  Numero: " << datos[i].get_telefono() << endl;
        cout << "\n  >  Saldo del cliente(a): " << datos[i].get_saldo() << endl;

        if (datos[i].get_moroso()){
			cout << "\n  >  La persona tiene deudas pendientes" << endl << endl;
		}
		
		else{
		    cout << "\n  >  La persona no tiene deudas" << endl << endl;
		}
    }
}
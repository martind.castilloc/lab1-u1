#include <iostream>

#include"F_contador.h"

using namespace std;

void menu(){
      
    int tam,opc;
    int i = 0;

    do{
		cout << "\t     ---------------------------- " << endl;
		cout << "\t     ***** MENÚ DE OPCIONES ***** " << endl;
		cout << "\t     ---------------------------- " << endl;
		cout << "\n  [1]  > Ingreso de frases." << endl;
		cout << "  [2]  > Salir del programa." << endl;
		cout << "\n - Favor, elija una opción: ";

		cin >> opc;
		cin.ignore();

        if (opc == 1){

        //Se define el tamaño del vector
	        cout << "Ingrese el largo de la frase" << endl;
	        cin >> tam;
	
	    //pasa como parametro el tamaño de la lista
	        F_contador p = F_contador(tam);
		    p.agregar_lista();
	        p.contar_letras();
	        p.resultados();
		    i++;

		} else if (opc == 2){
		
			cout << "\n\t¡Se ha cerrado el programa!\n" << endl;
			break;

		} else{
			
			cout << "\t--------------------------------------" << endl;
			cout << "\tVALOR INVÁLIDO " << endl;
			cout << "\t--------------------------------------\n" << endl;
		}
    } while(i<1);
}

int main(){

    menu();
	
    return 0;
};
#ifndef F_CONTADOR_H
#define F_CONTADOR_H

#include <iostream>

using namespace std;

class F_contador{

    private:
        int may,min,tam;
        char* frase = NULL;
        char letra;
    
    public:

        F_contador(int tam);
        void agregar_lista();
		void contar_letras();
		void resultados();

};
#endif